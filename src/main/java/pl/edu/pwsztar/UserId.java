package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        int sex = id.charAt(9);
        Optional<Sex> sexreturn;
        if(sex%2==0){
            sexreturn = Optional.of(Sex.WOMAN);
        }
        else {
            sexreturn = Optional.of(Sex.MAN);
        }
        return sexreturn;
    }

    @Override
    public boolean isCorrect() {
        //9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j
        int controlsum = (9*Character.getNumericValue(id.charAt(0)))
                +(7*Character.getNumericValue(id.charAt(1)))
                +(3*Character.getNumericValue(id.charAt(2)))
                +(Character.getNumericValue(id.charAt(3)))
                +(9*Character.getNumericValue(id.charAt(4)))
                +(7*Character.getNumericValue(id.charAt(5)))
                +(3*Character.getNumericValue(id.charAt(6)))
                +(Character.getNumericValue(id.charAt(7)))
                +(9*Character.getNumericValue(id.charAt(8)))
                +(7*Character.getNumericValue(id.charAt(9)));
        int rest = controlsum % 10;
        System.out.println(controlsum);
        System.out.println(rest);
        return rest == Character.getNumericValue(id.charAt(10));
    }

    @Override
    public Optional<String> getDate() {
        String day = id.substring(4,6);
        int rawmonth = Integer.parseInt(id.substring(2,4));
        String year = id.substring(0,2);
        String month = "";
        if(rawmonth>=81 && rawmonth<=92){
            month = parsemonth(rawmonth - 80);
            return Optional.of(day+"-"+ month +"-"+"18"+year);
        } else if(rawmonth>=1&&rawmonth<=9){
            month = parsemonth(rawmonth);
            return Optional.of(day+"-"+ month +"-"+"19"+year);
        } else if(rawmonth>=21&&rawmonth<=32){
            month = parsemonth(rawmonth - 20);
            return Optional.of(day+"-"+ month +"-"+"20"+year);
        } else if(rawmonth>=41&&rawmonth<=52){
            month = parsemonth(rawmonth - 40);
            return Optional.of(day+"-"+ month +"-"+"21"+year);
        } else if(rawmonth>=61&&rawmonth<=72){
            month = parsemonth(rawmonth - 60);
            return Optional.of(day+"-"+ month +"-"+"22"+year);
        }
        return Optional.empty();
    }

    private String parsemonth(int rawmonth){
        String month = String.valueOf(rawmonth);
        if(month.length()<2){
            month = "0" + month;
            return month;
        }
        return month;
    }
}
