package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size"() {
        given:
            def userid = new UserId(num);
        when:
            def isvalid = userid.isCorrectSize();
        then:
            isvalid == expected;
        where:
            num || expected
        "98081905751" || true
        "99121367751" || true
        "991021033"   || false
        ""            || false
    }

    @Unroll
    def "should get sex"(){
        given:
            def userId = new UserId(num);
        when:
            def sex = userId.getSex();
        then:
            sex.get() == expected;
        where:
            num || expected
        "98081905791" || UserIdChecker.Sex.MAN
        "99121367751" || UserIdChecker.Sex.MAN
        "98081905721" || UserIdChecker.Sex.WOMAN
    }

    @Unroll
    def "shoud check if number is valid"(){
        given:
            def userid = new UserId(num);
        when:
            def isvalid = userid.isCorrect();
        then:
            isvalid == expected;
        where:
        num || expected
        "44051401359" || true
        "98071805851" || true
        "44051401358" || false
    }

    @Unroll
    def "shoud return birth date based on pesel"(){
        given:
            def userid = new UserId(num);
        when:
            def date = userid.getDate();
        then:
            date.get() == expected;
        where:
        num     ||  expected
        "98071805851" || "18-07-1998"
        "02881805851" || "18-08-1802"
        "02281805851" || "18-08-2002"
    }


}
